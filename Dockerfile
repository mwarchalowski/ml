FROM continuumio/miniconda3

SHELL ["bash", "-c"]
RUN conda create -y -n ml numpy jupyterlab matplotlib pandas scipy scikit-learn tensorflow

CMD source activate ml && jupyter-lab --allow-root --ip="0.0.0.0"

